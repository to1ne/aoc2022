package main

import (
	"strings"
	"testing"
)

func TestOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int
	}{
		{
			input: `Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi`,
			expected: 31,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			result := one(reader)

			if result != tc.expected {
				t.Errorf("Expected %d, got %d", tc.expected, result)
			}
		})
	}
}
