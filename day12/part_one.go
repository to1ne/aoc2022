package main

// https://www.redblobgames.com/pathfinding/a-star/introduction.html

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type Pos struct {
	x, y int
}

func (p Pos) add(o Pos) Pos {
	return Pos{
		x: p.x + o.x,
		y: p.y + o.y,
	}
}

type Nav struct {
	heights [][]rune
	dest    Pos
	visited Visited
}

type Visited [][]int

func NewVisited(x, y int) Visited {
	visited := make([][]int, y)
	for i := range visited {
		visited[i] = make([]int, x)
	}
	return visited
}

func (v Visited) can(p Pos) bool {
	if p.x < 0 || p.y < 0 {
		return false
	}
	if p.x >= len(v[0]) || p.y >= len(v) {
		return false
	}
	return v[p.y][p.x] == 0
}

func (v Visited) at(p Pos) int {
	return v[p.y][p.x]
}

func (v Visited) print(me Pos) {
	for y, row := range v {
		for x, v := range row {
			if x == me.x && y == me.y {
				fmt.Printf("*")
			} else if v > 0 {
				fmt.Printf("#")
			} else {
				fmt.Printf(".")
			}
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n")
}

func (n *Nav) at(p Pos) rune {
	return n.heights[p.y][p.x]
}

func sign(v int) int {
	switch {
	case (v < 0):
		return -1
	case (v > 0):
		return 1
	}
	return 0
}

func abs(v int) int {
	return v * sign(v)
}

func heuristic(a, b Pos) int {
	return abs(a.x-b.x) + abs(a.y-b.y)
}

func (n *Nav) move(me Pos) int {
	minScore := -1

	for _, dir := range []Pos{
		{x: 1, y: 0},
		{x: 0, y: 1},
		{x: -1, y: 0},
		{x: 0, y: -1},
	} {
		next := me.add(dir)
		if !n.visited.can(next) {
			continue
		}

		if n.at(next) == 'E' {
			if n.at(me) == 'z' {
				return 1
			}
			continue
		}

		if n.at(me) == 'S' {
			if n.at(next) != 'a' {
				continue
			}
		} else if n.at(next)-n.at(me) > 1 {
			continue
		}

		//visited.print(next)

		score := n.move(next)
		if score < 0 {
			continue
		}

		if minScore < 0 || score < minScore {
			minScore = score + 1
		}
	}

	return minScore
}

func one(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	heights := [][]rune{}
	var me, dest Pos

	var y int
	for scanner.Scan() {
		line := []rune(scanner.Text())
		heights = append(heights, line)
		for x, r := range line {
			if r == 'S' {
				me = Pos{x: x, y: y}
			}
			if r == 'E' {
				dest = Pos{x: x, y: y}
			}
		}

		y++
	}

	nav := Nav{
		heights: heights,
		dest:    dest,
		visited: NewVisited(len(heights[0]), len(heights)),
	}

	return nav.move(me)
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}

// 793 is too high
