package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type pos struct {
	x, y int
}

func show(h, t, size pos) {
	for y := 0; y < size.y; y++ {
		l := strings.Repeat(".", size.x)

		var x int
		l = strings.Map(func(r rune) rune {
			defer func() { x++ }()

			if x == h.x && y == h.y {
				return 'H'
			} else if x == t.x && y == t.y {
				return 'T'
			}
			return r
		}, l)

		fmt.Println(l)
	}
	fmt.Println("")
}

func moveTail(h, t pos) pos {
	offX := h.x - t.x
	offY := h.y - t.y

	fmt.Println(offX, offY)

	if offX > 1 || offX < -1 {
		t.x += offX / 2
		t.y = h.y
	} else if offY > 1 || offY < -1 {
		t.y += offY / 2
		t.x = h.x
	}

	return t
}

func initial() (pos, pos, pos) {
	var h, t pos

	initial := `......
......
......
......
H.....`

	t = pos{x: -1, y: -1}

	lines := strings.Split(initial, "\n")
	for i, l := range lines {
		if x := strings.Index(l, "H"); x >= 0 {
			h = pos{x: x, y: i}
		}
		if x := strings.Index(l, "T"); x >= 0 {
			t = pos{x: x, y: i}
		}
	}
	if t.x < 0 {
		t.x = h.x
		t.y = h.y
	}

	size := pos{x: len(lines[0]), y: len(lines)}

	return h, t, size
}

func one(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	h, t, size := initial()

	fmt.Println(h, t, size)

	visited := make([][]bool, 1000)
	for i := range visited {
		visited[i] = make([]bool, 1000)
	}

	fmt.Println("==", "Initial State", "==")
	show(h, t, size)

	for scanner.Scan() {
		line := scanner.Text()
		dir, num, ok := strings.Cut(line, " ")
		if !ok {
			panic("failed to cut")
		}
		n, err := strconv.ParseInt(num, 10, 64)
		if err != nil {
			panic(err)
		}

		var move pos

		switch dir {
		case "R":
			move = pos{x: 1}
		case "L":
			move = pos{x: -1}
		case "U":
			move = pos{y: -1}
		case "D":
			move = pos{y: 1}
		}

		fmt.Println("==", line, "==")

		for i := 0; i < int(n); i++ {
			h.x += move.x
			h.y += move.y

			t = moveTail(h, t)

			show(h, t, size)
			visited[t.y+500][t.x+500] = true

		}
	}

	var count int
	for _, r := range visited {
		for _, p := range r {
			if p {
				count++
			}
		}
	}

	return count
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}
