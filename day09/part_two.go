package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"
)

const demo = !true

type pos struct {
	x, y int
}

func show(rope []pos, size pos, visited [][]bool) {
	if !demo {
		return
	}

	for y := 0; y < size.y; y++ {
		l := strings.Repeat(".", size.x)

		var x int
		l = strings.Map(func(r rune) rune {
			defer func() { x++ }()

			for i, p := range rope {
				if x == p.x && y == p.y {
					return rune('0' + i)
				}
			}
			if visited[y][x] {
				return '#'
			}
			return r
		}, l)

		fmt.Println(l)
	}
	fmt.Println("")
	time.Sleep(300 * time.Millisecond)
}

func sign(v int) int {
	switch {
	case (v < 0):
		return -1
	case (v > 0):
		return 1
	}
	return 0
}

func abs(v int) int {
	return v * sign(v)
}

func moveTails(h pos, tails []pos) []pos {
	t := tails[0]

	offX := t.x - h.x
	offY := t.y - h.y

	//fmt.Println(offX, offY)

	if 0 != offX && abs(offX) < abs(offY) {
		offX = 0
	} else if 0 != offY && abs(offY) < abs(offX) {
		offY = 0
	}

	t.x = h.x + sign(offX)
	t.y = h.y + sign(offY)

	if len(tails) == 1 {
		return []pos{h, t}
	}

	return append([]pos{h}, moveTails(t, tails[1:])...)
}

// more than 2317, less than 2389, and not 2340, 2326

const Len = 10

func two(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	size := pos{x: 1000, y: 1000}
	if demo {
		size = pos{x: 100, y: 60}
	}
	h := pos{x: size.x / 2, y: size.y / 2}

	rope := make([]pos, Len)

	for i := 0; i < Len; i++ {
		rope[i] = h
	}

	visited := make([][]bool, size.y)
	for i := range visited {
		visited[i] = make([]bool, size.x)
	}

	//fmt.Println("==", "Initial State", "==")
	show(rope, size, visited)

	for scanner.Scan() {
		line := scanner.Text()
		dir, num, ok := strings.Cut(line, " ")
		if !ok {
			panic("failed to cut")
		}
		n, err := strconv.ParseInt(num, 10, 64)
		if err != nil {
			panic(err)
		}

		var move pos

		switch dir {
		case "R":
			move = pos{x: 1}
		case "L":
			move = pos{x: -1}
		case "U":
			move = pos{y: -1}
		case "D":
			move = pos{y: 1}
		}

		//fmt.Println("==", line, "==")

		for i := 0; i < int(n); i++ {
			rope[0].x += move.x
			rope[0].y += move.y

			rope = moveTails(rope[0], rope[1:])

			show(rope, size, visited)
			visited[rope[Len-1].y][rope[Len-1].x] = true
		}
	}

	var count int
	for _, r := range visited {
		for _, p := range r {
			if p {
				count++
			}
		}
	}

	return count
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}
