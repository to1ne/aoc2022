package main

import (
	"strings"
	"testing"
)

func TestOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int
	}{
		{
			input: `R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2`,
			expected: 13,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			result := one(reader)

			if result != tc.expected {
				t.Errorf("Expected %d, got %d", tc.expected, result)
			}
		})
	}
}
