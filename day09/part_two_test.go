package main

import (
	"strings"
	"testing"
)

func TestTwo(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int
	}{
		{
			input: `R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2`,
			expected: 1,
		},
		{
			input: `R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20`,
			expected: 36,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			result := two(reader)

			if result != tc.expected {
				t.Errorf("Expected %d, got %d", tc.expected, result)
			}
		})
	}
}
