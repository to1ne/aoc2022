package main

import (
	"strings"
	"testing"
)

func TestTwo(t *testing.T) {
	reader := strings.NewReader(`vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`)
	expected := 70

	result := two(reader)

	if result != expected {
		t.Errorf("Expected %d, got %d", expected, result)
	}
}
