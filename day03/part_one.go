package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func findCommon(items []rune) rune {
	for i, v := range items {
		for _, w := range items[len(items)/2:] {
			if v == w {
				return v
			}
		}
		if i == len(items) {
			break
		}
	}
	panic("not found")

	return ' '
}

func runeScore(r rune) int {
	if r > 'Z' {
		return int(r - 'a' + 1)
	}

	return int(r - 'A' + 27)
}

func one(r io.Reader) int {
	var score int

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		all := []rune(scanner.Text())

		common := findCommon(all)

		score += runeScore(common)
	}

	return score
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}
