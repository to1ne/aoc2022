package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func findCommon(a, b, c []rune) rune {
	for _, v := range a {
		for _, w := range b {
			if v != w {
				continue
			}
			for _, u := range c {
				if w == u {
					return v
				}
			}
		}
	}
	panic("not found")

	return ' '
}

func runeScore(r rune) int {
	if r > 'Z' {
		return int(r - 'a' + 1)
	}

	return int(r - 'A' + 27)
}

func two(r io.Reader) int {
	var score int

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		a := []rune(scanner.Text())

		if !scanner.Scan() {
			panic("more expected")
		}
		b := []rune(scanner.Text())

		if !scanner.Scan() {
			panic("more expected")
		}
		c := []rune(scanner.Text())

		common := findCommon(a, b, c)

		score += runeScore(common)
	}

	return score
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}
