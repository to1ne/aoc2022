package main

import (
	"strings"
	"testing"
)

func TestOne(t *testing.T) {
	reader := strings.NewReader(`vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw`)
	expected := 157

	result := one(reader)

	if result != expected {
		t.Errorf("Expected %d, got %d", expected, result)
	}
}
