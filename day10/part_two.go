package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func doCycle(cycle, x int) (int, string) {
	cycle++

	fmt.Println(cycle, x)

	if cycle%40 == x || cycle%40 == x+1 || cycle%40 == x+2 {
		return cycle, "#"
	}
	return cycle, "."
}

func two(r io.Reader) string {
	scanner := bufio.NewScanner(r)

	var cycle int
	var screen string

	x := 1

	for scanner.Scan() {
		var sym string

		cycle, sym = doCycle(cycle, x)
		screen += sym
		if cycle%40 == 0 {
			screen += "\n"
		}

		line := scanner.Text()

		if line == "noop" {
			continue
		}

		_, val, ok := strings.Cut(line, " ")
		if !ok {
			panic(fmt.Sprintf("failed to cut: %v", line))
		}
		i, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			panic(err)
		}

		cycle, sym = doCycle(cycle, x)
		screen += sym
		if cycle%40 == 0 {
			screen += "\n"
		}

		x += int(i)
	}

	return screen
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}

// Not entirely correct, but close enough
// ####.####.###..####.#..#..##..#..#.###..
// ...#.#....#..#.#....#..#.#..#.#..#.#..#.
// ..#..###..###..###..####.#....#..#.#..#.
// .#...#....#..#.#....#..#.#.##.#..#.###.#
// #....#....#..#.#....#..#.#..#.#..#.#....
// ####.#....###..#....#..#..###..##..#....
