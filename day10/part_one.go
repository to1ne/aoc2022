package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func one(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	var signal, cycle int

	at := 20
	x := 1

	for scanner.Scan() {
		cycle++
		// fmt.Println(cycle, x, signal)

		if cycle == at {
			signal += x * cycle
			// fmt.Println("signal", signal, x, cycle, at, x*cycle)
			at += 40
		}

		line := scanner.Text()
		// fmt.Println(line)

		if line == "noop" {
			continue
		}

		_, val, ok := strings.Cut(line, " ")
		if !ok {
			panic(fmt.Sprintf("failed to cut: %v", line))
		}
		i, err := strconv.ParseInt(val, 10, 64)
		if err != nil {
			panic(err)
		}

		cycle++
		// fmt.Println(cycle, x, signal)

		if cycle == at {
			signal += x * cycle
			// fmt.Println("signal", signal, x, cycle, at, x*cycle)
			at += 40
		}

		x += int(i)
	}

	return signal
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}
