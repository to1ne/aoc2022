package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Item struct {
	value int
}

type Monkey struct {
	items       []int
	inspected   int
	op          func(int) int
	test        func(int) bool
	trueTarget  int
	falseTarget int
}

func (m *Monkey) inspect(ind int, divisor int, monkeys *[]Monkey) {
	for _, i := range m.items {
		m.inspected++
		new := m.op(i)
		new = new % divisor
		var target int
		if m.test(new) {
			target = m.trueTarget
		} else {
			target = m.falseTarget
		}
		((*monkeys)[target]).items = append(((*monkeys)[target]).items, new)
	}
}

func two(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	var monkeys []Monkey
	monkey := Monkey{items: []int{}}

	divisor := 1

	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), " ")

		if line == "" {
			monkeys = append(monkeys, monkey)
			monkey = Monkey{items: []int{}}
			continue
		}
		before, after, ok := strings.Cut(line, ":")
		if !ok {
			panic("cannot cut on colon")
		}

		if before == "Starting items" {
			for _, item := range strings.Split(after, ",") {
				i, err := strconv.Atoi(strings.Trim(item, " "))
				if err != nil {
					panic(err)
				}
				monkey.items = append(monkey.items, i)
			}
		}

		if before == "Operation" {
			_, factor, ok := strings.Cut(after, "*")
			if ok {
				if strings.Trim(factor, " ") == "old" {
					monkey.op = func(v int) int { return v * v }
					continue
				}
				i, err := strconv.Atoi(strings.Trim(factor, " "))
				if err != nil {
					panic(err)
				}
				monkey.op = func(v int) int { return v * i }
				continue
			}

			_, factor, ok = strings.Cut(after, "+")
			if ok {
				if strings.Trim(factor, " ") == "old" {
					monkey.op = func(v int) int { return v + v }
					continue
				}
				i, err := strconv.Atoi(strings.Trim(factor, " "))
				if err != nil {
					panic(err)
				}
				monkey.op = func(v int) int { return v + i }
				continue
			}
		}

		if before == "Test" {
			_, factor, ok := strings.Cut(after, "divisible by")
			if ok {
				i, err := strconv.Atoi(strings.Trim(factor, " "))
				if err != nil {
					panic(err)
				}
				divisor = divisor * i

				monkey.test = func(v int) bool { return (v % i) == 0 }
				continue
			}
		}
		if before == "If true" {
			_, target, ok := strings.Cut(after, "throw to monkey")
			if ok {
				i, err := strconv.Atoi(strings.Trim(target, " "))
				if err != nil {
					panic(err)
				}
				monkey.trueTarget = i
				continue
			}
		}
		if before == "If false" {
			_, target, ok := strings.Cut(after, "throw to monkey")
			if ok {
				i, err := strconv.Atoi(strings.Trim(target, " "))
				if err != nil {
					panic(err)
				}
				monkey.falseTarget = i
				continue
			}
		}
	}
	monkeys = append(monkeys, monkey)

	for round := 0; round < 10000; round++ {
		for i, m := range monkeys {
			m.inspect(i, divisor, &monkeys)
			m.items = []int{}
			monkeys[i] = m
		}
		fmt.Printf("== After round %d ==\n", round+1)
		for i, m := range monkeys {
			fmt.Println("Monkey", i, "inspected items", m.inspected, "times")
		}
	}

	// for _, m := range monkeys {
	// 	fmt.Println(m.inspected)
	// }

	return 0
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}
