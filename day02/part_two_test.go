package main

import (
	"strings"
	"testing"
)

func TestTwo(t *testing.T) {
	reader := strings.NewReader("A Y\nB X\nC Z")

	result := two(reader)

	if result != 12 {
		t.Errorf("Expected 12, got %d", result)
	}
}
