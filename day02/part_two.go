package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

const (
	rock int = iota // 0
	paper
	scissors
)

func letter2Shape(s string) int {
	v := int(([]rune(s))[0] - 'A')

	return v
}

func shape2letter(s int) string {
	v := string([]rune{rune('A' + s)})

	return v
}

func shapeScore(s string) int {
	return letter2Shape(s) + 1
}

func roundScore(they, us string) int {
	t := letter2Shape(they)
	u := letter2Shape(us)
	if t == u {
		return 3
	}
	if u-t == 1 || t-u == 2 {
		return 6
	}
	return 0
}

func choose(strat, they string) string {
	offset := int(([]rune(strat))[0] - 'Y')
	us := letter2Shape(they) + offset

	letter := shape2letter((us + 3) % 3)

	return letter
}

func two(r io.Reader) int {
	var score int

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		they, strat, ok := strings.Cut(scanner.Text(), " ")
		if !ok {
			panic("failed to cut")
		}
		us := choose(strat, they)

		fmt.Println("round: ", roundScore(they, us), shapeScore(us))
		score += roundScore(they, us) + shapeScore(us)
	}

	return score
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}
