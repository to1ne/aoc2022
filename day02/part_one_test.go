package main

import (
	"strings"
	"testing"
)

func TestOne(t *testing.T) {
	reader := strings.NewReader("A Y\nB X\nC Z")

	result := one(reader)

	if result != 15 {
		t.Errorf("Expected 15, got %d", result)
	}
}
