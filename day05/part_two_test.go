package main

import (
	"strings"
	"testing"
)

func TestTwo(t *testing.T) {
	reader := strings.NewReader(`    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2`)
	expected := "MCD"

	result := two(reader)

	if result != expected {
		t.Errorf("Expected %s, got %s", expected, result)
	}
}
