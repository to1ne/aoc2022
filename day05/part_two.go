package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func two(r io.Reader) string {
	var tops string

	scanner := bufio.NewScanner(r)

	var stackLines []string
	var count int

	for scanner.Scan() {
		line := scanner.Text()

		if strings.Contains(line, "1") {
			nums := strings.Fields(line)
			c, err := strconv.ParseInt(strings.TrimSpace(nums[len(nums)-1]), 10, 64)
			if err != nil {
				panic(err)
			}
			count = int(c)
			continue
		}

		if line == "" {
			break
		}

		stackLines = append(stackLines, line)
		continue
	}

	stacks := make([][]string, count)

	for _, line := range stackLines {
		for i := 0; i < count; i++ {
			if (i*4 + 3) > len(line) {
				break
			}
			crate := line[i*4 : i*4+3]
			if crate[0:1] == "[" {
				stacks[i] = append(stacks[i], crate[1:2])
			}
		}
	}

	for scanner.Scan() {
		instr := strings.Split(scanner.Text(), " ")
		if len(instr) != 6 {
			panic("invalid instruction")
		}
		num, err := strconv.ParseInt(instr[1], 10, 64)
		if err != nil {
			panic(err)
		}
		from, err := strconv.ParseInt(instr[3], 10, 64)
		if err != nil {
			panic(err)
		}
		from-- // this is an index now
		to, err := strconv.ParseInt(instr[5], 10, 64)
		if err != nil {
			panic(err)
		}
		to-- // this is an index now

		moving := make([]string, num)
		copy(moving, stacks[from][:num])

		stacks[to] = append(moving, stacks[to]...)
		stacks[from] = stacks[from][num:]
	}

	for _, s := range stacks {
		tops += s[0]
	}

	return tops
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}
