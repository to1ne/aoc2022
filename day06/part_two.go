package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

const Len = 14

func unique(needle, haystack string) bool {
	if strings.Contains(haystack, needle) {
		return false
	}
	if len(haystack) == 1 {
		return true
	}
	return unique(haystack[0:1], haystack[1:])
}

func two(r io.Reader) int {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()

		for i := 0; i < len(line)-Len; i++ {
			if unique(line[i:i+1], line[i+1:i+Len]) {
				return i + Len
			}
		}
	}
	return -1
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}
