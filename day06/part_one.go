package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func one(r io.Reader) int {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()

		for i := 0; i < len(line)-4; i++ {
			if line[i] == line[i+1] ||
				line[i] == line[i+2] ||
				line[i] == line[i+3] ||
				line[i+1] == line[i+2] ||
				line[i+1] == line[i+3] ||
				line[i+2] == line[i+3] {
				continue
			}
			return i + 4
		}
	}
	return -1
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}
