package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	var cur, total int
	var elfes []int

	for scanner.Scan() {
		l := scanner.Text()
		if l == "" {
			elfes = append(elfes, cur)
			cur = 0
			continue
		}
		i, err := strconv.ParseInt(l, 10, 64)
		if err != nil {
			panic(err)
		}
		cur += int(i)
	}

	sort.Ints(elfes)

	for _, v := range elfes[len(elfes)-3:] {
		fmt.Println(v)
		total += v
	}

	fmt.Println(total)
}
