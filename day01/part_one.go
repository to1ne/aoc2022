package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	var max, cur int64

	for scanner.Scan() {
		l := scanner.Text()
		if l == "" {
			if max < cur {
				max = cur
			}
			cur = 0
			continue
		}
		i, err := strconv.ParseInt(l, 10, 64)
		if err != nil {
			panic(err)
		}
		cur += i
	}
	fmt.Println(max)
}
