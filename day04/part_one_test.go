package main

import (
	"strings"
	"testing"
)

func TestOne(t *testing.T) {
	reader := strings.NewReader(`2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8`)
	expected := 2

	result := one(reader)

	if result != expected {
		t.Errorf("Expected %d, got %d", expected, result)
	}
}
