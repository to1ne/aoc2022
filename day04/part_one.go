package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func rangeOf(s string) []int {
	first, second, ok := strings.Cut(s, "-")
	if !ok {
		panic("range not valid")
	}
	a, err := strconv.ParseInt(first, 10, 64)
	if err != nil {
		panic(err)
	}
	b, err := strconv.ParseInt(second, 10, 64)
	if err != nil {
		panic(err)
	}

	return []int{int(a), int(b)}
}

func fullOverlap(first, second string) bool {
	r1 := rangeOf(first)
	r2 := rangeOf(second)
	if r2[0] < r1[0] {
		r1, r2 = r2, r1
	}
	return r2[1] <= r1[1] || r2[0] == r1[0]
}

func one(r io.Reader) int {
	var score int

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		first, second, ok := strings.Cut(scanner.Text(), ",")
		if !ok {
			panic("pairs not valid")
		}
		if fullOverlap(first, second) {
			score++
		}
	}

	return score
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}
