package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func two(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	trees := make([][]int64, 0)

	for scanner.Scan() {
		line := scanner.Text()
		row := make([]int64, len(line))
		for i, c := range []rune(line) {
			v, err := strconv.ParseInt(string(c), 10, 64)
			if err != nil {
				panic(err)
			}
			row[i] = v
		}
		trees = append(trees, row)
	}

	var maxScore int

	for i, row := range trees {
		for j, t := range row {
			score := 1

			var vis int

			//  On the left
			for k := j; k >= 0; k-- {
				if k == j {
					continue
				}
				vis++
				if t <= row[k] {
					break
				}
			}
			if vis == 0 {
				continue
			}
			score *= vis
			vis = 0

			//  On the right
			for k := j; k < len(row); k++ {
				if k == j {
					continue
				}
				vis++
				if t <= row[k] {
					break
				}
			}
			if vis == 0 {
				continue
			}
			score *= vis
			vis = 0

			//  On the top
			for k := i; k >= 0; k-- {
				if k == i {
					continue
				}
				vis++
				if t <= trees[k][j] {
					break
				}
			}
			if vis == 0 {
				continue
			}
			score *= vis
			vis = 0

			//  On the bottom
			for k := i; k < len(trees); k++ {
				if k == i {
					continue
				}
				vis++
				if t <= trees[k][j] {
					break
				}
			}
			if vis == 0 {
				continue
			}
			score *= vis

			if score > maxScore {
				maxScore = score
			}
		}
	}

	return maxScore
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(two(f))
}
