package main

import (
	"strings"
	"testing"
)

func TestOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int
	}{
		{
			input: `30373
25512
65332
33549
35390`,
			expected: 21,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			result := one(reader)

			if result != tc.expected {
				t.Errorf("Expected %d, got %d", tc.expected, result)
			}
		})
	}
}
