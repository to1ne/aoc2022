package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func one(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	trees := make([][]int64, 0)

	for scanner.Scan() {
		line := scanner.Text()
		row := make([]int64, len(line))
		for i, c := range []rune(line) {
			v, err := strconv.ParseInt(string(c), 10, 64)
			if err != nil {
				panic(err)
			}
			row[i] = v
		}
		trees = append(trees, row)
	}

	var visible int

	for i, row := range trees {
		for j, t := range row {
			//  From the left
			hidden := false

			for k, o := range row[:j] {
				if j == k {
					continue
				}
				if t <= o {
					hidden = true
					break
				}
			}
			if !hidden {
				visible++
				continue
			}

			//  From the right
			hidden = false
			for k, o := range row[j:] {
				if k == 0 {
					continue
				}
				if t <= o {
					hidden = true
					break
				}
			}
			if !hidden {
				visible++
				continue
			}

			//  From the top
			hidden = false
			for k := 0; k < i; k++ {
				if t <= trees[k][j] {
					hidden = true
					break
				}
			}
			if !hidden {
				visible++
				continue
			}

			//  From the bottom
			hidden = false
			for k := len(trees) - 1; k > i; k-- {
				if t <= trees[k][j] {
					hidden = true
					break
				}
			}
			if !hidden {
				visible++
				continue
			}
		}
	}

	return visible
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}
