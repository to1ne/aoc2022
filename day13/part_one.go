package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

// https://cs.gmu.edu/~sean/lisp/cons/

type Comparable interface {
	Compare(Comparable) int
	Car() Comparable
	Cdr() Comparable
	Print() string
}

type Num struct {
	val int
}

func (n *Num) Car() Comparable {
	return n
}

func (n *Num) Cdr() Comparable {
	return nil
}

func (n *Num) Last() bool {
	return true
}

func (n *Num) Compare(them Comparable) int {
	for {
		if m, ok := them.(*Num); ok {
			if n.val < m.val {
				return -1
			}
			if m.val < n.val {
				return 1
			}
			return 0
		}
		if them.Car() == nil {
			return 1
		}
		them = them.Car()
	}

	return 0
}

func (n *Num) Print() string {
	return fmt.Sprintf("%d", n.val)
}

type Cons struct {
	first Comparable
	rest  Comparable
}

func (c *Cons) Car() Comparable {
	return c.first
}

func (c *Cons) Cdr() Comparable {
	return c.rest
}

func (c *Cons) Last() bool {
	return true
}

// Compare returns 0 when both are equal.
// Negative when us is less than them.
// Positive when them is greater than us.
func (c *Cons) Compare(them Comparable) int {
	us := Comparable(c)

	for us.Car() != nil {
		if them.Car() == nil {
			return 1
		}

		if result := us.Car().Compare(them.Car()); result != 0 {
			return result
		}

		if us.Cdr() == nil {
			return -1
		}
		if them.Cdr() == nil {
			return 1
		}

		us = us.Cdr()
		them = them.Cdr()
	}

	return 0
}

func (c *Cons) Print() string {
	result := "["
	cur := Comparable(c)
	sep := ""

	for cur.Car() != nil {
		result += sep + cur.Car().Print()

		if cur.Cdr() == nil {
			break
		}

		cur = cur.Cdr()
		sep = ","
	}
	result += "]"

	return result
}

func parse(line string) (Comparable, string) {
	if len(line) == 0 {
		return nil, ""
	}
	if line[0] == ']' {
		return nil, line[1:]
	}
	if line[0] == '[' || line[0] == ',' {
		first, after := parse(line[1:])
		rest, after := parse(after)

		return &Cons{
			first: first,
			rest:  rest,
		}, after
	}

	ind := strings.IndexAny(line, ",]")
	before := line
	var after string
	if ind >= 0 {
		before = line[:ind]
		after = line[ind:]
	}

	val, err := strconv.Atoi(before)
	if err != nil {
		panic(err)
	}

	return &Num{val: val}, after
}

func one(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	var sum int
	ind := 1
	for scanner.Scan() {
		line := scanner.Text()

		if line == "" {
			continue
		}
		fmt.Println("== Pair", ind, "==")

		left, rest := parse(line)
		if len(rest) > 0 {
			panic(fmt.Sprintf("some rest at left: %v", rest))
		}
		fmt.Println(left.Print())

		if !scanner.Scan() {
			panic("no right")
		}
		line = scanner.Text()
		right, rest := parse(line)
		if len(rest) > 0 {
			panic(fmt.Sprintf("some rest at right: %v", rest))
		}
		fmt.Println(right.Print())

		if left.Compare(right) <= 0 {
			fmt.Println("Right order")
			sum += ind
		} else {
			fmt.Println("Not right order")
		}
		ind++
	}

	return sum
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}

// 4229 is too low
// 5583 is too low
