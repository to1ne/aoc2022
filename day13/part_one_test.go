package main

import (
	"strings"
	"testing"
)

func TestOne(t *testing.T) {
	for _, tc := range []struct {
		input    string
		expected int
	}{
		{
			input: `[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]`,
			expected: 13,
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			reader := strings.NewReader(tc.input)
			result := one(reader)

			if result != tc.expected {
				t.Errorf("Expected %d, got %d", tc.expected, result)
			}
		})
	}
}

func TestOne_parsePrint(t *testing.T) {
	for _, tc := range []struct {
		input string
		//expected int
	}{
		{
			input: "[1,2,3,4]",
		},
		{
			input: "[[1],[2,3,4]]",
		},
		{
			input: "[[4,4],4,4]",
		},
	} {
		t.Run(tc.input, func(t *testing.T) {
			cons, _ := parse(tc.input)
			result := cons.Print()

			if result != tc.input {
				t.Errorf("Expected %s, got %s", tc.input, result)
			}
		})
	}
}
