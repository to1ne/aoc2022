package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type statEntry struct {
	name    string
	dir     bool
	sz      int
	entries []*statEntry
	parent  *statEntry
}

func (e *statEntry) size() int {
	if !e.dir {
		return e.sz
	}

	var size int

	for _, entry := range e.entries {
		size += entry.size()
	}

	return size
}

func walkDir(tree *statEntry) int {
	var total int

	for _, entry := range tree.entries {
		if entry.dir {
			total += walkDir(entry)
		}
	}

	sz := tree.size()
	if sz <= 100000 {
		total += sz
	}

	return total
}

func one(r io.Reader) int {
	scanner := bufio.NewScanner(r)

	tree := &statEntry{dir: true}
	cwd := tree

	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())

		// It's a command
		if fields[0] == "$" {
			if fields[1] == "cd" {
				if fields[2] == ".." {
					cwd = cwd.parent
				} else if fields[2] == "/" {
					cwd = tree
				} else {
					for _, sub := range cwd.entries {
						if sub.name == fields[2] {
							cwd = sub
							break
						}
					}
				}
			}
			continue
		}
		// It's ls output
		if fields[0] == "dir" {
			cwd.entries = append(cwd.entries,
				&statEntry{
					name:   fields[1],
					dir:    true,
					parent: cwd,
				})
		} else {
			size, err := strconv.ParseInt(fields[0], 10, 64)
			if err != nil {
				panic(err)
			}

			cwd.entries = append(cwd.entries,
				&statEntry{
					name: fields[1],
					sz:   int(size),
				})
		}
	}

	return walkDir(tree)
}

func main() {
	f, err := os.Open("input.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fmt.Println(one(f))
}
